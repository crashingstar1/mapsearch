import React, { useState } from 'react'

import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import Search from './pages/Search'
import SearchResult from './pages/SearchResult'

function App() {
 return(
<Router>
  <div>
<Routes>

      <Route path="/search" exact Component={Search}/>
      <Route path="/searchresults" exact Component={SearchResult}/>
</Routes>
  
  </div>
</Router>
 )
}

export default App
