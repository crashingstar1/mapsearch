import React from 'react'
import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import { Navigation } from 'swiper/modules';

const images = ['src/assets/image_230619155524618.jpg', 'src/assets/image_230619160541650.jpg']

export default function SwiperComponent() {
    return (
        <Swiper navigation modules={[Navigation]}>
           <SwiperSlide><img src='src/assets/image_230619155524618.jpg'/></SwiperSlide>
           <SwiperSlide><img src='src/assets/image_230619160541650.jpg'/></SwiperSlide>
        </Swiper>
    )
}