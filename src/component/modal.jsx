import React, { useState } from 'react';
import { CloseCircleOutline } from 'react-ionicons'

function Modal({ closeModal }) {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [mobile, setMobile] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();
    // Perform your form submission here
    // You can use the 'name', 'email', and 'mobile' state values
  };

  return (
    <div className="modal fade" id="coupon_wrap">
      <div className="modal-dialog modal-dialog-centered" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h4 className="modal-title" id="myModalLabel">Download Pricing</h4>
            <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={closeModal}>
              <span aria-hidden="true"><CloseCircleOutline/></span>
            </button>
          </div>
          <div className="modal-body">
            <div className="modal-coupon-code">
              <div id="pricing_message" className="alert alert-dismissible fade" style={{ display: 'none', wordBreak: 'break-word' }}></div>
              <form onSubmit={handleSubmit} id="new_pricing_form">
                <div className="store-content">
                  <div className="form-control-wrap">
                    <div className="form-group" style={{ paddingBottom: 0 }}>
                      <label htmlFor="name">Name</label>
                      <input type="text" className="form-control" name="name" placeholder="Enter your name" id="name" value={name} onChange={(e) => setName(e.target.value)} />
                    </div>
                    <div className="form-group" style={{ paddingBottom: 0 }}>
                      <label htmlFor="email">Email Address</label>
                      <input type="email" className="form-control" name="email" placeholder="Enter the email to receive the pricing" id="email" value={email} onChange={(e) => setEmail(e.target.value)} />
                    </div>
                    <div className="form-group" style={{ paddingBottom: 0 }}>
                      <label htmlFor="mobile">Contact Number</label>
                      <input type="text" className="form-control" name="mobile" placeholder="Your contact number" id="mobile" value={mobile} onChange={(e) => setMobile(e.target.value)} />
                      <input type="hidden" className="form-control" name="media_id" value="4231" id="media_id" />
                    </div>
                  </div>
                </div>
                <div className="coupon-bottom pt-0">
                  <div className="text-center">
                    <button type="submit" className="btn v1 btnsubmitform" style={{ width: '100%' }}>Submit</button>
                  </div>
                  <button id="checkLoginBtn" onClick={() => check_need_login()} style={{ display: 'none' }} type="button">
                    checkLoginBtn
                  </button>
                </div>
              </form>
              <div className="welcomemessagemedia" style={{ display: 'none' }}>
                <div className="modal-body">
                  <div className="modal-coupon-code">
                    <form>
                      <div className="store-content">
                        <div className="form-control-wrap">
                          <p>Thanks for signing up! We just need you to verify your email address to complete setting up your account.</p>
                        </div>
                      </div>
                      <div className="coupon-bottom pt-0 mb-3">
                        <div className="row justify-content-center">
                          <div className="col-md-6">
                            <div className="text-center">
                              <button
                                type="button"
                                className="btn v1 btnsubmitform"
                                style={{ width: '100%' }}
                                onClick={() => send_verifyemailmedia()}
                              >
                                Verify My Email
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div className="verifyyouremailmedia" style={{ display: 'none' }}>
                <div className="modal-body">
                  <div className="modal-coupon-code">
                    <div className="form-control-wrap">
                      <p>
                        We have sent an email to{' '}
                        <a href="javascript:;" style={{ color: '#2196f3', textDecoration: 'underline' }} id="emailarreaddmedia"></a>
                        Please click the link in that email to continue.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Modal;
