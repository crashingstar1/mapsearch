export default function Footer() {
return (<footer id="footer">
<div className="footer-content">
    <div className="container">
        <div className="row">
            <div className="col-lg-6">
                <div className="widget">
                    <div className="widget-title"><img className="logo-default footerLogo" style={{width:'200px'}} src="src/assets/footer_logo.png"/></div>
                    <p className="mb-5">
                        Novus Media Sdn. Bhd. is an outdoor media<br />
                        speacialist that focuses on out-of-home <br />
                        advertising with sites available nationwide
                    </p>
                </div>
            </div>
            <div className="col-lg-6">
                <div className="row">
                    <div className="col-lg-6">
                        <div className="widget">
                            <div className="widget-title">Novus Media</div>
                            <ul className="list">
                                <li><a href="../../index.html">Home</a></li>
                                <li><a href="../../aboutus.html">About</a></li>
                                <li><a href="../../services.html">Services</a></li>
                                <li><a href="../../contactus.html">Contact</a></li>
                            </ul>
                        </div>
                    </div>

                    <div className="col-lg-6">
                        <div className="widget">
                            <div className="widget-title">Novus Media</div>
                            <ul className="list">
                                <li><a href="mailto:vivien@novusmedia.com.my">Email : vivien@novusmedia.com.my</a></li>
                                <li><a href="tel:+60123691991">Tel : +6012-3691991</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div className="copyright-content">
    <div className="container">
        <div className="copyright-text text-center">&copy; 2020 Novus Media. All Rights Reserved</div>
    </div>
</div>
</footer>)
}