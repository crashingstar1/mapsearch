import React, { useState } from 'react'

import "leaflet/dist/leaflet.css"
import '../plugin.css'
import '../App.css'
import '../custom.css'
import { MapContainer, TileLayer } from 'react-leaflet'
import Dropdown from '../component/dropdown'
import Footer from '../component/footer'
import { Link } from 'react-router-dom'
import InfiniteScroll from "react-infinite-scroll-component";

function Search() {
  const page_first = "0"
  const page_last = "1024"
  const total_count = "1024"
  const mediaList = [{
    media_id: 1,
    tag: 'Overhead Bridge',
    sku_name: '1 sided panel Overhead Bridge at Jalan Bangsar before LRT Bangsar Station, Bangsar, Kuala Lumpur',
    sku_area: "Jalan Bangsar before LRT Bangsar Station, Bangsar, Kuala Lumpur",
    display_width: "40",
    display_height: "40",
  }, {
    media_id: 1,
    tag: 'Overhead Bridge',
    sku_name: '1 sided panel Overhead Bridge at Jalan Bangsar before LRT Bangsar Station, Bangsar, Kuala Lumpur',
    sku_area: "Jalan Bangsar before LRT Bangsar Station, Bangsar, Kuala Lumpur",
    display_width: "40",
    display_height: "40",
  }, {
    media_id: 1,
    tag: 'Overhead Bridge',
    sku_name: '1 sided panel Overhead Bridge at Jalan Bangsar before LRT Bangsar Station, Bangsar, Kuala Lumpur',
    sku_area: "Jalan Bangsar before LRT Bangsar Station, Bangsar, Kuala Lumpur",
    display_width: "40",
    display_height: "40",
  }, {
    media_id: 1,
    tag: 'Overhead Bridge',
    sku_name: '1 sided panel Overhead Bridge at Jalan Bangsar before LRT Bangsar Station, Bangsar, Kuala Lumpur',
    sku_area: "Jalan Bangsar before LRT Bangsar Station, Bangsar, Kuala Lumpur",
    display_width: "40",
    display_height: "40",
  },
  {
    media_id: 1,
    tag: 'Overhead Bridge',
    sku_name: '1 sided panel Overhead Bridge at Jalan Bangsar before LRT Bangsar Station, Bangsar, Kuala Lumpur',
    sku_area: "Jalan Bangsar before LRT Bangsar Station, Bangsar, Kuala Lumpur",
    display_width: "40",
    display_height: "40",
  },


  ]

  const [area, setArea] = useState('')


  const handleAreaChange = (event) => {
    setArea(event.target.value);
  };

  const handleFormSubit = (event) => {
    console.log(area)
  };

  const [items, setItems] = useState(mediaList);
  const [hasMore, setHasMore] = useState(true);

  const fetchMoreData = () => {
    if (items.length >= 500) {
      setHasMore(false);
      return;
    }
    // Simulate a fake async API call to fetch more data
    setTimeout(() => {
      setItems((prevItems) => prevItems.concat(mediaList));
    }, 500);
  };

  return (
    <>
      <div className="filter-wrapper style1 mar-top-100 half-map special-margin" scroll ng-controller="checkout" ng-cloak>
        <div className="container-fluid">
          <div className="row" style={{ height: '1000px' }}>
            <div className="col-lg-6 col-md-12">
              <MapContainer
                center={[0, 0]}
                zoom={6}
              >
                <TileLayer
                  url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                  attribution="Map data &copy; <a href=&quot;https://openstreetmap.org&quot;>OpenStreetMap</a> contributors"
                />
              </MapContainer>
            </div>
            <div className="col-lg-6 col-md-12" >
              <div className="form-group filter-group mb-0">
                <div className="row" >
                  <div className="col-12">
                    <form onSubmit={handleFormSubit}>
                      <div className="filter-sub-menu style1">
                        <ul>
                          <li className="has-sub">
                            <div className="clearfix"></div>
                            <ul className="explore__form-checkbox-list half-filter" style={{ display: 'block' }}>
                              <div className="col-md-12 float-left">
                                <div className="clearfix"></div>
                                <div className="row">
                                  <div className="col-lg-5 mb-md-4 mb-2">
                                    <label className="labeldisplay" style={{ marginTop: 0 }}>Area</label>
                                    <input className="hero__form-input custom-select" onChange={handleAreaChange} type="text" name="sku_area" id="sku_area" placeholder="Where are you looking for?" ng-model="filter.sku_area" />
                                  </div>

                                  <div className="clearfix"></div>

                                  <div className="col-lg-5 mb-md-4 mb-2" style={{ float: 'left' }}>
                                    <div className="dropdown-container">
                                      <div id="list1" className="dropdown-check-list" tabIndex="100">
                                        <label className="labeldisplay" style={{ marginTop: 0 }}> Media Type </label>
                                        <div>
                                          <Dropdown />
                                        </div>
                                      </div>
                                    </div>

                                  </div>
                                  <div className="clearfix"></div>

                                  <div className="col-sm-1 text-right sm-left float-left pt-1">
                                    <button className="btn v1 mt-md-4 mt-0 mb-3 mb-md-0" type='submit'>Apply</button>
                                  </div>

                                  <div className="clearfix"></div>

                                  <div className=" col-lg-12 col-md-12">
                                    <div className="panel panel-default">
                                      <div className="panel-body"><strong className="mr-2">Selected</strong>

                                      </div>
                                    </div>
                                  </div>
                                  <div className="clearfix"></div>
                                </div>
                              </div>

                            </ul>
                          </li>
                        </ul>
                      </div>
                    </form>
                  </div>
                </div>
                <div className=" mx-2">
                  <div className="row my-4 align-items-center">

                    <div className="col-lg-12 col-sm-12 col-12">
                      <div className="item-element res-box  text-left xs-left">
                        <p style={{ lineHeight: 'normal', marginTop: '-3px', marginBottom: '-2px' }}>
                          Showing
                          {total_count > 0 && ` ${page_first}-${page_last} of ${total_count}`}
                          {total_count === 0 && `${total_count}`} Listings
                        </p>
                        {total_count > 0 && (
                          <style>
                            {`.new-footer .sec_footer { margin-top: 0; }`}
                          </style>
                        )}

                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="d-flex" style={{ overflow: 'auto', maxHeight: "800px" }}>

                <InfiniteScroll
                  dataLength={items.length}
                  next={fetchMoreData}
                  hasMore={hasMore}
                  loader={<h4>Loading...</h4>}

                  endMessage={
                    <p style={{ textAlign: "center" }}>
                      <b>Yay! You have seen it all</b>
                    </p>
                  }
                >
                  <div className="overflow-auto boardlisting">
                    {items.map(item => (
                      <div key={item.media_id} className="card mx-2 mb-2" style={{ maxWidth: '1000px' }}>
                        <div className="row g-0">
                          <div className="col-md-5 mt-3 mb-3 pr-md-0 pr-3">
                            <a href="searchresult.html" className="pl-md-3 pl-0">
                              <img src="src/assets/image_230221203933918.jpg" alt={item.sku_name} />
                            </a>
                          </div>
                          <div className="col-md-7">
                            <div className="card-body">
                              <p className="badge badge-success badge-outlined mb-2" style={{ color: '#1F1F1F' }}>
                                {item.tag}
                              </p>
                              <h4>
                                <a href="searchresult.html">{item.sku_name}</a>
                              </h4>
                              <p className="card-text mb-3 mb-md-3">{item.sku_area}</p>
                              <div className="row align-items-end displaySize">
                                <div className="col-md-8">
                                  <p className="card-text text-success mb-0">
                                    Display Size : {item.display_width} ft x {item.display_height} ft
                                  </p>
                                </div>
                              </div>
                              <div className="clearfix"></div>
                              <Link to="/searchresults" >
                                <button className="moredetail btn btn-sm btn-success" style={{ fontSize: '13px' }}>
                                  View Details
                                </button>

                              </Link>
                            </div>
                            <div className="clearfix"></div>
                            <div className="scrollmore">
                              <div className="loading">
                                <div className="line"></div>
                                <div className="line"></div>
                                <div className="line"></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    ))}
                    <div className="clearfix"></div>
                  </div>

                </InfiniteScroll>
              </div>
            </div>
          </div>
        </div>
      </div >
      <Footer />
    </>
  )
}

export default Search
