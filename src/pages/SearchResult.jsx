import React, { useState } from 'react'
import "leaflet/dist/leaflet.css"
import '../plugin.css'
import '../App.css'
import '../custom.css'
import { MapContainer, TileLayer } from 'react-leaflet'
import { ArrowBack, LocationOutline, ShareSocialOutline, ArrowForward } from 'react-ionicons'
import Footer from '../component/footer'
import SwiperComponent from '../component/swiper'
import Modal from '../component/modal'

function SearchResult() {

    const [isModalOpen, setIsModalOpen] = useState(false);

    // Function to open the modal
    const openModal = () => {
      setIsModalOpen(true);
    };
  
    // Function to close the modal
    const closeModal = () => {
      setIsModalOpen(false);
    };


    return (
        <>
        {isModalOpen&& <Modal closeModal={closeModal}/>}
        <div className="list-details-section section-padding mt-5">
            <div className="container-fluid">
                <div className="row">
                    <div className="reversecol">
                    
                        <div className="col-lg-5 col-md-12">
                            <div className="d-sm-block d-md-none d-lg-none">
                                <p style={{ lineHeight: '25px', color: '#000' }}><span>Jalan Bangsar before LRT Bangsar Station, Bangsar, Kuala Lumpur</span></p>
                                <div className="clearfix"></div>
                                <h2>1 sided panel Overhead Bridge at Jalan Bangsar before LRT Bangsar Station, Bangsar, Kuala Lumpur</h2>
                                <div className="clearfix"></div>
                                <span className="mediatype">Overhead Bridge</span>

                            </div>

                            <div id="gallery" className="list-details-section mb-4">
                                <SwiperComponent/>
                            </div>

                            <div className="clearfix"></div>

                            <div className="mt-10" style={{ position: 'relative', overflow: 'hidden', height: '350px', paddingTop: "100px" }}>
                                <div className="map_panelSideLeft">



                                    <MapContainer
                                        center={[0, 0]}
                                        zoom={6}
                                    >
                                        <TileLayer
                                            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                                            attribution="Map data &copy; <a href=&quot;https://openstreetmap.org&quot;>OpenStreetMap</a> contributors" />
                                    </MapContainer>
                                </div>
                            </div>

                            <div className="clearfix"></div>

                        </div>

                        <div className="col-md-7">
                            <div className="list-details-wrap">
                                <div className="list-details-section">
                                    <div className="single-listing-title float-left">
                                        <div className="d-none d-sm-none d-md-block">
                                            <p style={{ lineHeight: '25px', color: '#000' }}>
                                                <LocationOutline />
                                                <span>Jalan Bangsar before LRT Bangsar Station, Bangsar, Kuala Lumpur</span>
                                            </p>
                                            <div className="clearfix"></div>
                                            <h2>1 sided panel Overhead Bridge at Jalan Bangsar before LRT Bangsar Station, Bangsar, Kuala Lumpur</h2>
                                            <div className="clearfix"></div>
                                            <span className="mediatype">Overhead Bridge</span>
                                        </div>
                                        <div className="clearfix"></div>
                                        <div className="detialAds">
                                            <h2>MEDIA INFORMATION</h2>
                                            <div className="row">
                                                <div className="col-md-4 col-xs-4 col-4">
                                                    <h3>Media Panel</h3>
                                                    <div className="descDetail">
                                                        <span style={{ color: '#337bb3' }}>
                                                            <strong>1</strong>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div className="col-md-4 col-xs-4 col-4">
                                                    <h3>Display size (H x W)</h3>
                                                    <div className="descDetail">
                                                        <span style={{ color: '#337bb3' }}>
                                                            <strong>10.00ft</strong> x <strong>40.00ft</strong>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div className="col-md-4 col-xs-4 col-4">
                                                    <h3>Lighting</h3>
                                                    <div className="descDetail">
                                                        <span style={{ color: '#337bb3' }}>
                                                            <strong>-</strong>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-md-4 col-xs-4 col-4">
                                                    <h3>GPS Coordinate</h3>
                                                    <div className="descDetail">
                                                        <span style={{ color: '#337bb3' }}>
                                                            <strong>3.123169</strong> ,{' '}
                                                            <span style={{ color: '#337bb3' }}>
                                                                <strong>101.676285</strong>
                                                            </span>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div className="col-md-4 col-xs-4 col-4">
                                                    <h3>Traffic volume</h3>
                                                    <div className="descDetail">
                                                        <span style={{ color: '#0071BD', fontSize: '16px', fontWeight: 600 }}>
                                                            <strong>1,000,000/
                                                                <span style={{ fontSize: '16px', color: '#555', fontWeight: 500 }}>month</span>
                                                            </strong>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div className="col-md-4 col-xs-4 col-4">
                                                    <h3>Media Availability</h3>
                                                    <div className="descDetail">
                                                        <span style={{ color: '#337bb3', fontSize: '16px' }}>
                                                            <strong>Subject to availability</strong>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="detialAds">
                                            <h2>DESCRIPTION</h2>
                                            <div className="row">
                                                <div className="col-md-12">
                                                    <div className="descDetail">
                                                        <p>
                                                            Located along Jalan Bangsar before LRT Bangsar Station
                                                            <br />
                                                            <br /> Direction from NPE Highway / Abdullah Hukum heading towards Bangsar / KL City Centre
                                                            <br />
                                                            <br /> 400m to Dataran Maybank
                                                            <br /> 650m to LRT Bangsar
                                                            <br /> 1.6km to Bangsar Village Mall
                                                            <br /> 2km to Muzium Negara
                                                            <br /> 2.5km to KL Sentral / NU Sentral
                                                            <br /> 3km to Bangsar Shopping Centre
                                                            <br /> &nbsp;
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="detialAds" style={{ display: 'none' }}>
                                            <h2>Categories</h2>
                                            <div className="row">
                                                <div className="col-md-12">
                                                    <h3 style={{ color: '#5A5A5A', fontFamily: 'Poppins', fontSize: '16px', fontWeight: 500 }}>
                                                        Target audience
                                                    </h3>
                                                    <div className="descDetail">
                                                        <span style={{ color: '#337bb3', fontSize: '16px' }}>
                                                            <strong></strong>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="detialAds" style={{ display: 'none' }}>
                                            <h2>OTHER INFORMATION</h2>
                                            <div className="row">
                                                <div className="col-md-12 col-xs-12 col-12">
                                                    <h3>Telco tower applicable</h3>
                                                    <div className="descDetail">
                                                        <span style={{ color: '#337bb3', fontSize: '16px' }}>
                                                            <strong>No</strong>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-md-6 col-xs-6 col-6">
                                                    <h3>License</h3>
                                                    <div className="descDetail">
                                                        <span style={{ color: '#337bb3', fontSize: '16px' }}>
                                                            <strong>-</strong>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div className="col-md-6 col-xs-6 col-6">
                                                    <h3>Land contract</h3>
                                                    <div className="descDetail">
                                                        <span style={{ color: '#337bb3', fontSize: '16px' }}>
                                                            <strong>-</strong>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-md-6 col-xs-6 col-6">
                                                    <h3>Public liability insurance</h3>
                                                    <div className="descDetail">
                                                        <span style={{ color: '#337bb3', fontSize: '16px' }}>
                                                            <strong>-</strong>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-auto">
                                    <div className="shareinlineButton">
                                        <div className="sharethis-inline-share-buttons"></div>
                                    </div>
                                    <div className="share-btnsocial">
                                    
                                        <button className='downloadpricelist' onClick={openModal}>Download Pricing</button>
                                        <div style={{ position: 'relative', display: 'inline-block' }}>
                                            <a href="javascript:;" className="socialshare">
                                                <ShareSocialOutline /> Share
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="clearfix"></div>
                    </div>


                </div>
                <div className="clearfix">
                    <div className="similar-listing">
                        <div className="similar-listing-title">
                            <h3>
                                <strong>Media Nearby</strong>
                            </h3>
                        </div>

                        <div className="row">
                            <div className="col-md-3">
                                <div className="detail">
                                    <a href="searchresult.html">
                                        <img src="src/assets/media_221229103621177.jpg" className="img-fluid" alt="Media 1" />
                                        <h1>2 sided Overhead Bridge Billboard at Jalan Bangsar</h1>
                                        <a href="searchresult.html" className="readmore">
                                            Read More
                                        </a>
                                    </a>
                                </div>
                            </div>
                            <div className="col-md-3">
                                <div className="detail">
                                    <a href="searchresult.html">
                                        <img src="src/assets/media_221229103621177.jpg" className="img-fluid" alt="Media 2" />
                                        <h1>2 sided Overhead Bridge Billboard at Jalan Bangsar</h1>
                                        <a href="searchresult.html" className="readmore">
                                            Read More
                                        </a>
                                    </a>
                                </div>
                            </div>
                            <div className="col-md-3">
                                <div className="detail">
                                    <a href="searchresult.html">
                                        <img src="src/assets/media_221229103621177.jpg" className="img-fluid" alt="Media 3" />
                                        <h1>2 sided Overhead Bridge Billboard at Jalan Bangsar</h1>
                                        <a href="searchresult.html" className="readmore">
                                            Read More
                                        </a>
                                    </a>
                                </div>
                            </div>
                            <div className="col-md-3">
                                <div className="detail">
                                    <a href="searchresult.html">
                                        <img src="src/assets/media_221229103621177.jpg" className="img-fluid" alt="Media 4" />
                                        <h1>2 sided Overhead Bridge Billboard at Jalan Bangsar</h1>
                                        <a href="searchresult.html" className="readmore">
                                            Read More
                                        </a>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><Footer /> </>
    )
}

export default SearchResult
